/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Reserva;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoParte;

/**
 *
 * @author kevin Figueroa
 */
@Stateless
@LocalBean
public class ReservaFacade implements Serializable {

//    public EntityManagerFactory emf = Persistence.createEntityManagerFactory("flota_unit");
//    public EntityManager em = emf.createEntityManager();
    @PersistenceContext(unitName = "pu_flota")
    public EntityManager em;

    /**
     *
     * @param idTipoVehiculo Integer identificador del tipo de vehiculo a buscar
     * @return el numero de reservas para un determinado tipo de vehiculo
     */
    public int countByIdTipoVehiculo(final Integer idTipoVehiculo) {

        long valor;
        if (em != null) {

            if (idTipoVehiculo != null || idTipoVehiculo > 0) {

                Query query = em.createQuery("SELECT COUNT(r) FROM Reserva r JOIN r.idVehiculo v "
                        + "JOIN v.idModelo mo JOIN mo.idTipoVehiculo tv WHERE tv.idTipoVehiculo=:id");
               query.setParameter("id", idTipoVehiculo);
                valor = (Long) query.getSingleResult();

            } else {
                throw new IllegalArgumentException("parametro invalido !");
            }
        } else {
            throw new IllegalStateException("EntityManager nulo");
        }

        return (int) valor;

    }

    /**
     *
     *
     * @param diasAtras dias previos que se desean conocer
     * @return lista con las reservas echas el numero de dias atras que resive
     * como parametro ordenados Ascendentemente
     */
    public List<sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Reserva> findReservasUltimosDias(int diasAtras) {

        List<Reserva> lista = new ArrayList<>();

        if (em != null) {

            if (diasAtras > 0) {

                LocalDateTime date = LocalDateTime.now().plusDays(-diasAtras);
                DateTimeFormatter formato = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss", Locale.ENGLISH);
                String fecha = formato.format(date);

                Query query = em.createQuery("SELECT r FROM Reserva r JOIN r.idVehiculo v"
                        + " WHERE r.fechaReserva >= " + "{ts '" + fecha + "'} ORDER BY v.idVehiculo, r.fechaReserva ASC ");

                lista = query.getResultList();

            } else {
                throw new IllegalArgumentException("parametro invalido !");
            }
        } else {
            throw new IllegalStateException("EntityManager nulo");
        }

        return lista;

    }

    /*
    PARCIAL DIA VIERNES
     */
    //tipoParte mas popular por marca de vehiculo
    //
    public TipoParte findMasUsadaPorIdMarca(final Integer idMarca) {

        TipoParte tp = new TipoParte();
        if (em != null) {
            if (idMarca != null || idMarca > 0) {
                
                Query q = em.createQuery("SELECT tp FROM ModeloParte mp JOIN mp.idModelo.idMarca m "
                        + "JOIN mp.idParte.idSubTipoParte.idTipoParte tp"
                        + " WHERE m.idMarca=:id ORDER BY COUNT(tp.idTipoParte) DESC");
               q.setParameter("id", idMarca);
               tp=(TipoParte) q.getSingleResult();

            } else {
                throw new IllegalArgumentException("parametro invalido !");
            }
        } else {
            throw new IllegalStateException("EntityManager nulo");
        }

        return tp;

    }

    /**
     * Metodo de 6 puntasos parcial dia Viernes
     *
     * @param anio año para verificar las reservas
     * @return map con los clientes con mas reservas aprobadas mensuales
     */
    public Map<Integer, Object[]> findTopClientePorAnioAprobado(int anio) {

        Map<Integer, Object[]> map = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        if (em != null) {
            if (anio > 0) {
                List<Object[]> lista;
                Object object[];
                String cliente;
                long cantidadReservas;
                //object[] de un elemento con dos un arreglo object[] con 2 elementos cliente cantidad de reservas
                int mes;
                for (mes = 0; mes <= 11; mes++) {
                    cal.set(anio, (mes + 1), 1);
                    int ultimoDiaMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

                    Query q = em.createQuery("SELECT r.cliente,Count(r.cliente) AS res FROM Reserva r JOIN r.estadoReservaList er"
                            + " JOIN er.idTipoEstadoReserva ter WHERE ter.indicaAprobacion=1 AND r.fechaReserva "
                            + "BETWEEN '" + anio + "-" + (mes + 1) + "-01' AND '" + anio + "-" + (mes + 1) + "-" + ultimoDiaMes + "' ORDER BY res DESC, r.cliente ASC ");

                    lista = q.getResultList();
                    for (Object[] ls : lista) {
                        cliente = (String) ls[0];
                        cantidadReservas = (long) ls[1];
                        if (cliente == null) {
                            cliente = "_";
                        }
                        object = new Object[]{cliente, cantidadReservas};
                        map.put(mes, object);
                    }
                }
            } else {
                throw new IllegalArgumentException("Parametro debe de ser mayor a cero");
            }
        } else {
            throw new IllegalStateException("EntityManager nulo !!");
        }
        return map;
    }

}
