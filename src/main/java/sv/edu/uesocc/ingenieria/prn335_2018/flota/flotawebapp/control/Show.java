/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Marca;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Reserva;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoParte;

/**
 *
 * @author kevin Figueroa
 */
@WebServlet(name = "Show", urlPatterns = {"/Show"})
public class Show extends HttpServlet {

    @Inject
    ReservaFacade rf;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Show</title>");
            out.println("</head>");
            out.println("<body>");
            Integer n = 1;
            int count = rf.countByIdTipoVehiculo(n);

            out.print("la cantidad de reservas del vehiculo con id= " + n + " es_");
            out.println(" " + count);
            out.println("<br/> ");
            out.println("<br/> ");
            out.println("<br/> ");
            out.println("segundo item----> ");
            List<Reserva> ls = rf.findReservasUltimosDias(360);
            out.println("" + ls.size());
            out.println("<br/> ");
            out.println("RESERVAs:");
            out.println("<br/> ");
            for (Reserva l : ls) {

                out.println(l.getIdVehiculo());
                out.println(l.getCliente());
                out.println(l.getFechaReserva());

            }

            out.println("<br/> ");
            out.println("-----");
            out.println("<h1>Servlet Show at " + request.getContextPath() + "</h1>");
            out.println("<br/> ");

            TipoParte tp = new TipoParte();
            int marca = 1;
            tp = rf.findMasUsadaPorIdMarca(marca);

            out.println("El tipo de parte mas popular de la marca " + marca + " es: " + tp.getNombre());

            out.println("<br/> ");

            int anio = 2018;
            Map<Integer, Object[]> mp = rf.findTopClientePorAnioAprobado(anio);

            List<Integer> lsi = new ArrayList<>(mp.keySet());
            List<Object[]> lsob = new ArrayList<>(mp.values());

            for (Integer integer : lsi) {
                out.println("<br/> ");
                out.println("mes: " + integer);

            }

            for (Object[] objects : lsob) {
                out.println("<br/> ");
                out.println("Cliente:    " + objects[0]);

                out.println("                   Cantidad de Reservas: " + objects[1]);

            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
